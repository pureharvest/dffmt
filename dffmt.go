package dffmt

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"regexp"
	"strings"
)

type Options struct {
	LineEnding string
	TabWidth   int
	TabIndent  bool
	Fragment   bool
	Debug      bool
}

func (o Options) Indent(s string) string {

	if o.TabIndent {
		return "\t" + s
	}

	return strings.Repeat(" ", o.TabWidth) + s
}

func (o Options) Dedent(s string) string {

	if o.TabIndent {
		return s[:len(s)-1]
	}

	if len(s) <= o.TabWidth {
		return ""
	}
	return s[:len(s)-o.TabWidth]
}

var scopeStartCommands = []string{
	"Class",
	"Deferred_View",
	"DFBeginHeader",
	"DFCreate_Menu",
	"Enum_List",
	"Enumeration_List",
	"Function",
	"Object",
	"Procedure",
	"Procedure_Section",
	"Item_List",
}
var scopeEndCommands = []string{
	"End_Class",
	"Cd_End_Object",
	"DFEndHeader",
	"End_Pull_Down",
	"End_Menu",
	"End_Enum_List",
	"End_Enumeration_List",
	"End_Function",
	"End_Object",
	"End_Procedure",
	"End_Item_List",
}

var indentRegex *regexp.Regexp
var dedentRegex *regexp.Regexp
var imageRegex = regexp.MustCompile("^\\/[^ *\\/]")

func init() {
	indentString := "(?i)(^[ \\t]*\\b(" + strings.Join(scopeStartCommands, "|") + ")\\b|(^|\\s)begin($|\\s))"
	indentRegex = regexp.MustCompile(indentString)
	dedentString := "(?i)(^[ \\t]*\\b(" + strings.Join(scopeEndCommands, "|") + ")\\b|(^|\\s)end($|\\s))"
	dedentRegex = regexp.MustCompile(dedentString)
}

func imageStart(line string) bool {
	return imageRegex.MatchString(line)
}

func imageEnd(line string) bool {
	return strings.HasPrefix(line, "/*")
}

func needsIndent(line string) bool {

	return !strings.HasPrefix(line, "//") && indentRegex.MatchString(line)
}

func needsDedent(line string) bool {

	return !strings.HasPrefix(line, "//") && dedentRegex.MatchString(line)
}

func Process(filename string, src []byte, opt *Options) ([]byte, error) {

	if opt == nil {
		opt = &Options{
			LineEnding: "\r\n",
			TabWidth:   2,
			TabIndent:  false,
		}
	}

	var prefix string
	var inImage bool
	var res bytes.Buffer
	lineNum := 1
	s := bufio.NewScanner(bytes.NewBuffer(src))

	for s.Scan() {
		inImage = imageStart(s.Text()) || (inImage && !imageEnd(s.Text()))

		line := strings.TrimLeft(s.Text(), " \t")

		if inImage {
			line = s.Text()
		} else if needsIndent(line) {
			if opt.Debug {
				log.Printf("Indent at line %d. (%s)", lineNum, line)
			}
			line = fmt.Sprintf("%s%s", prefix, line)
			prefix = opt.Indent(prefix)
		} else if needsDedent(line) {
			if opt.Debug {
				log.Printf("Dedent at line %d. (%s)", lineNum, line)
			}
			if len(prefix) < opt.TabWidth {
				return nil, fmt.Errorf("tried to dedent past start %s:%d: (%s)", filename, lineNum, line)
			}

			prefix = opt.Dedent(prefix)
			line = fmt.Sprintf("%s%s", prefix, line)
		} else {
			line = fmt.Sprintf("%s%s", prefix, line)
		}

		if _, err := fmt.Fprintf(&res, "%s%s", line, opt.LineEnding); err != nil {
			return nil, err
		}
		lineNum++
	}

	if err := s.Err(); err != nil {
		return nil, err
	}

	return bytes.TrimRight(res.Bytes(), opt.LineEnding), nil
}