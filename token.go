package dffmt

type Token int

const (
	ILLEGAL Token = iota
	EOF
	COMMENT

	literal_beg
	// Identifiers and basic type literals
	// (these tokens stand for classes of literals)
	IDENT  // main
	INT    // 12345
	NUM    // 123.45
	REAL   // 1.55e003 1.55 x 10 to the power of 3, or 1,550
	HEX    // $1F
	STRING // "abc"

	// By default, Visual DataFlex represents dates in the following format:
	// mm/dd/yyyy, where mm represents the month (1-12), dd the day (1-31 or
	// 	whatever the maximum day is for a given month) and yyyy is the year (0001 –
	// 	2200).
	// 	Visual DataFlex can be configured to use European dates in the format
	// 	dd/mm/yyyy or military dates in the format yyyy.mm.dd.
	DATE
	literal_end

	operator_beg
	// Operators and delimiters
	ADD // +
	SUB // -
	MUL // *
	QUO // /
	MIN // Min
	MAX // Max

	LAND // And
	LOR  // Or

	EQL // =
	LSS // <
	GTR // >
	NOT // Not

	NEQ      // <>
	LEQ      // <-
	GEQ      // >=
	CONTAINS // Contains
	MATCHES  // Matches

	// Bitwise operators
	AND // land
	OR  // lor

	LPAREN // (
	LBRACK // [
	LBRACE // {
	COMMA  // ,
	PERIOD // .

	RPAREN    // )
	RBRACK    // ]
	RBRACE    // }
	SEMICOLON // ;
	COLON     // :
	operator_end
)
