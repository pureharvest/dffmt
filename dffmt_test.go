package dffmt_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pureharvest/dffmt"
)

func Test_Format(t *testing.T) {

	tests := []struct {
		name   string
		input  []byte
		output []byte
	}{
		{
			"objects",
			[]byte(`
object is a array
string test 0
end_object
`),
			[]byte(`
object is a array
  string test 0
end_object
`),
		},
		{
			"multiple",
			[]byte(`
object is a array
string test 0
	procedure test
	date a_date
	end_procedure
end_object
`),
			[]byte(`
object is a array
  string test 0
  procedure test
    date a_date
  end_procedure
end_object
`),
		},
		{
			"begin statements",
			[]byte(`
begin
test
asd
end
`),
			[]byte(`
begin
  test
  asd
end
`),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			res, err := dffmt.Process("", test.input, nil)

			if err != nil {
				t.Fatal("Unexpected error: ", err)
			}

			assert.Equal(t, string(test.output), string(res))

		})
	}
}

func runFileTest(t *testing.T, path string) {
	inputPath := filepath.Join(path, "input.txt")
	expectedPath := filepath.Join(path, "output.txt")

	if _, err := os.Stat(inputPath); os.IsNotExist(err) {
		t.Fatal("input file doesn't exist for ", path)
	}

	if _, err := os.Stat(expectedPath); os.IsNotExist(err) {
		t.Fatal("expected file doesn't exist for", path)
	}

	input, err := ioutil.ReadFile(inputPath)

	if err != nil {
		t.Fatal(err)
	}

	expected, err := ioutil.ReadFile(expectedPath)

	if err != nil {
		t.Fatal(err)
	}

	res, err := dffmt.Process(inputPath, input, nil)

	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, strings.Split(string(expected), "\n"), strings.Split(string(res), "\n"))

}

// isDir reports whether name is a directory.
func isDir(name string) bool {
	fi, err := os.Stat(name)
	return err == nil && fi.IsDir()
}

func Test_Files(t *testing.T) {

	filepath.Walk("./test-fixtures/", func(path string, f os.FileInfo, err error) error {
		if err == nil && f.IsDir() && f.Name() != "test-fixtures" {
			t.Run(f.Name(), func(t *testing.T) {
				runFileTest(t, path)
			})
		}
		if err != nil {
			t.Fatal(err)
		}
		return nil
	})
}